(function(){
	function ajax(options){
		var request = new XMLHttpRequest();
		request.open(options.method, options.url, true);

		request.onload = function() {
		  if (request.status >= 200 && request.status < 400) {
		    var data = JSON.parse(request.responseText);
		    options.success(data);
		  } else {
		    options.success(data);
		  }
		};

		request.onerror = function() {
		  options.error.apply(null,arguments);
		};
		if(options.method==='POST')
			request.send(options.data);
		else
			request.send();
	}
	window.ajax=ajax;
})();

