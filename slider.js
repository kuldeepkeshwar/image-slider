(function(){
	function initSlideshow(element,data){
		var ul,imageNumber,imageWidth,currentPostion,currentImage;

		
		function initSliderState(){
			currentPostion = 0;
			currentImage = 0;
		}
		function init(el){
			var _li_items; 
			ul = el.querySelector('.image_slider');
			li_items = ul.children;
			imageNumber = li_items.length;
			imageWidth = li_items[0].children[0].clientWidth;
			ul.style.width = parseInt(imageWidth * imageNumber) + 'px';
			
			el.querySelector(".prev").onclick = function(){ 
				onClickPrev();
			};
			el.querySelector(".next").onclick = function(){ 
				onClickNext();
			};
		}

		function moveTo(destImage){
			var _dir,_jumpCount,_opts;
			_jumpCount = Math.abs(destImage - currentImage);
			_dir = currentImage > destImage ? 1 : -1;
			currentPostion = -1 * currentImage * imageWidth;
			_opts = {
				duration:1000,
				delay:1,
				partail:function(p){
					return p;
				},
				step:function(p){
					ul.style.left = parseInt(currentPostion + _dir * p * imageWidth * _jumpCount) + 'px';
				},
				callback:function(){
					currentImage = destImage;
				}	
			};
			animate(_opts);
		}

		function onClickPrev(){
			if (currentImage == 0){
				moveTo(imageNumber - 1);
			}else{
				moveTo(currentImage - 1);
			}		
		}

		function onClickNext(){
			if (currentImage == imageNumber - 1){
				moveTo(0);
			}else{
				moveTo(currentImage + 1);
			}		
		}
		initSliderState();
		initTemplate(element,data,function(){
			init(element);				
		});
		
	}
	function initTemplate(element,data,cb){
		var _ul,_li,_fragment,i;
		element.innerHTML=getSliderTemplate(data.title);
		_ul=element.querySelector('.image_slider');
		_fragment=document.createDocumentFragment();
		for(i=0;i<data.images.length;i++){
			if(i==0){
				_li=getSliderItemTemplate(data.images[i].path,data.images[i].caption,cb);	
			}else{
				_li=getSliderItemTemplate(data.images[i].path,data.images[i].caption);
			}			
			_fragment.appendChild(_li);
		}	
		_ul.appendChild(_fragment);
	}
	function getSliderTemplate(title){
		return "<div class='container'><p class='text-center'>"+title+"</p><div class='slider_wrapper'><ul class='image_slider'></ul></div><button class='prev'><- prev</button><button class='next'>next -></button></div>";
	}
	function getSliderItemTemplate(src,text,cb){
		var _li,_img,_p;
		
		_li = document.createElement("li");
		_img = document.createElement("img");
		_p = document.createElement("h6");
		_img.src=src;
		if(cb){
			_img.onload=function(){
				cb();
			}	
		}
		
		if(_p.classList){
			_p.classList.add('text-center');
		}else{
			_p.className='text-center';
		}
		_p.textContent=text;
		
		_li.appendChild(_img);
		_li.appendChild(_p);
		return _li;
	}
	function animate(opts){
		var _start = new Date();
		var _id = setInterval(function(){
			var _diffTime,_progress;
			_diffTime = new Date() - _start;
			_progress = _diffTime / opts.duration;
			if (_progress > 1){
				_progress = 1;
			}
			opts.step(opts.partail(_progress));
			if (_progress == 1){
				clearInterval(_id);
				opts.callback();
			}
		}, opts.delay || 16);	
	}
	window.initSlideshow=initSlideshow;
})();


